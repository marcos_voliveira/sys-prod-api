package com.example.sysprodapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SysprodApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SysprodApiApplication.class, args);
	}

}
