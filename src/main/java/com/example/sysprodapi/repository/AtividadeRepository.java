package com.example.sysprodapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.sysprodapi.model.Atividade;

public interface AtividadeRepository extends JpaRepository<Atividade, Integer> {

}
