package com.example.sysprodapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.sysprodapi.model.Tipo;

public interface TipoRepository extends JpaRepository<Tipo, Integer>{

}
