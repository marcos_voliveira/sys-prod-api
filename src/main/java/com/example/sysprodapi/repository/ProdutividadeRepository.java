package com.example.sysprodapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.sysprodapi.model.Produtividade;

public interface ProdutividadeRepository extends JpaRepository<Produtividade, Integer> {

}
