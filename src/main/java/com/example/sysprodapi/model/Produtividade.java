package com.example.sysprodapi.model;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "produtividade")
public class Produtividade {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id_produtividade;
	private int id_atividade;
	private int id_tipo;
	private Date data_produtividade;
	private String num_processo;
	private short quantidade;
	
	public int getId_produtividade() {
		return id_produtividade;
	}
	public void setId_produtividade(int id_produtividade) {
		this.id_produtividade = id_produtividade;
	}
	public int getId_atividade() {
		return id_atividade;
	}
	public void setId_atividade(int id_atividade) {
		this.id_atividade = id_atividade;
	}
	public int getId_tipo() {
		return id_tipo;
	}
	public void setId_tipo(int id_tipo) {
		this.id_tipo = id_tipo;
	}
	
	public Date getData_produtividade() {
		return data_produtividade;
	}
	public void setData_produtividade(Date data_produtividade) {
		this.data_produtividade = data_produtividade;
	}
	public String getNum_processo() {
		return num_processo;
	}
	public void setNum_processo(String num_processo) {
		this.num_processo = num_processo;
	}
	public short getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(short quantidade) {
		this.quantidade = quantidade;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id_produtividade;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Produtividade other = (Produtividade) obj;
		if (id_produtividade != other.id_produtividade)
			return false;
		return true;
	}
	
	
}
