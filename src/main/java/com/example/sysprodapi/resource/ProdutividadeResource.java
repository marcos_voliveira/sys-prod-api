package com.example.sysprodapi.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.sysprodapi.model.Produtividade;
import com.example.sysprodapi.repository.ProdutividadeRepository;



@RestController
@RequestMapping("/produtividades")
public class ProdutividadeResource {
	@Autowired
	private ProdutividadeRepository produtividadeRepository;
	
	@GetMapping
	public List<Produtividade> listarProdutividades(){
		return this.produtividadeRepository.findAll();
	}
}
