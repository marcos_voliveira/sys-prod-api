package com.example.sysprodapi.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.sysprodapi.model.Atividade;
import com.example.sysprodapi.repository.AtividadeRepository;

@RestController
@RequestMapping("/atividades")
public class AtividadeResource {
	@Autowired
	private AtividadeRepository atividadeRepository;
	
	@GetMapping
	public List<Atividade> listarAtividades() {
		return this.atividadeRepository.findAll();
	}
}
