package com.example.sysprodapi.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.sysprodapi.model.Tipo;
import com.example.sysprodapi.repository.TipoRepository;

@RestController
@RequestMapping("/tipos")
public class TipoResource {
	@Autowired
	private TipoRepository tipoRepository;
	
	@GetMapping
	public List<Tipo> listarTipos(){
		return this.tipoRepository.findAll();
	}
}
