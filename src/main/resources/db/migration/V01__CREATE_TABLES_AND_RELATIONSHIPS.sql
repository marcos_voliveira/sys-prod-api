CREATE TABLE atividade(
	id_atividade INT(8) PRIMARY KEY AUTO_INCREMENT,
	descricao_atividade VARCHAR(50)
) ENGINE= InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE tipo(
	id_tipo INT(8) PRIMARY KEY AUTO_INCREMENT,
	descricao_tipo VARCHAR(50)
) ENGINE= InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE produtividade(
	id_produtividade INT(8) PRIMARY KEY AUTO_INCREMENT,
	id_atividade INT(8),
	id_tipo INT(8),
	data_produtividade DATETIME,
	num_processo VARCHAR(20),
	quantidade TINYINT(3)
) ENGINE= InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE tipo ADD CONSTRAINT UK_descricao_tipo UNIQUE (descricao_tipo);
ALTER TABLE atividade ADD CONSTRAINT UK_descricao_atividade UNIQUE (descricao_atividade);

ALTER TABLE produtividade ADD CONSTRAINT FK_produtividade_atividade FOREIGN KEY (id_atividade) REFERENCES atividade(id_atividade);
ALTER TABLE produtividade ADD CONSTRAINT FK_produtividade_tipo FOREIGN KEY (id_tipo) REFERENCES tipo(id_tipo);